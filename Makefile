ifneq ($(shell uname -s),Linux)
$(warning This project only works on Linux-based operating systems!)
endif

ifeq ($(KERNELDIR),)
ifneq ($(realpath ./linux),)
KERNELDIR ?= ./linux
else
KERNELDIR ?= /lib/modules/$(shell uname -r)/build
endif
endif
KERNELDIR := $(abspath $(KERNELDIR))

V ?= 0
export V

IPROUTE2_TAG := v4.19.0

IPROUTE2_L2AUTH := ./src/iproute2-l2auth
export L2AUTH_DIR := $(abspath ./src/l2auth)/
export IPROUTE2_DIR := $(abspath ./src/iproute2-l2auth/iproute2-src)/

.PHONY: all
all: l2auth iproute2 ;

.PHONY: install
install: l2auth/install iproute2/install ;

l2auth/%:
	@$(MAKE) $(MFLAGS) -C '$(L2AUTH_DIR)' \
		$(if $(wildcard $(POSTPROC)),POSTPROC='$(abspath $(POSTPROC))') '$*'
l2auth: l2auth/all

$(IPROUTE2_DIR):
	git clone -b '$(IPROUTE2_TAG)' git://git.kernel.org/pub/scm/network/iproute2/iproute2.git '$@'

iproute2/clean:
	@$(MAKE) $(MFLAGS) -C '$(IPROUTE2_L2AUTH)' clean
iproute2/%: $(IPROUTE2_DIR)
	@$(MAKE) $(MFLAGS) -C '$(IPROUTE2_L2AUTH)' '$*'
iproute2: iproute2/all

.PHONY: clean
clean: l2auth/clean iproute2/clean ;


help:
	@echo
	@echo 'Layer 2 Authentication'
	@echo '----------------------'
	@echo
	@echo 'Targets:'
	@echo '    all                   Builds both the kernel module and iproute2 link extension'
	@echo '    l2auth                Builds the l2auth kernel module'
	@echo '    iproute2              Builds the iproute2 link extension'
	@echo '    install               Installs both the kernel module and iproute2 link extension'
	@echo '    clean                 Cleans all the build files'
	@echo
	@echo '    clone_linux           Clones the recommended linux kernel into the root directory'
	@echo '    clone_iproute2        Clones the recommended iproute2 into the root directory'
	@echo
	@echo 'Environment Variables:'
	@echo '    KERNELDIR             The root directory of the kernel sources'
	@echo '    IPROUTE2_TAG          The iproute2 tag to clone (only relevant to `make clone_iproute2`)'
	@echo
