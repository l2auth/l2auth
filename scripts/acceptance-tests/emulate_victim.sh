#!/bin/sh -e

ATTACKER_IP="10.0.1.1"
ATTACKER_MAC="3c:97:0e:40:58:3c"
DEVICE='eth0'
L2AUTH_DEV='l2a0'
IP='10.0.1.2'

KEY=1234567890123456789012345678901234567890123456789012345678901234

ip addr flush dev "${DEVICE}"
ip -6 addr flush dev "${DEVICE}"

modprobe l2auth
ip link add link "${DEVICE}" "${L2AUTH_DEV}" type l2auth key "${KEY}"
ip addr add "${IP}/24" dev "${L2AUTH_DEV}"
ip link set "${DEVICE}" up
ip link set "${L2AUTH_DEV}" up

arp -s "${ATTACKER_IP}" "${ATTACKER_MAC}"

sleep 2
dmesg --clear

after_nc() {
  echo 'The dmesg:'
  dmesg --level=warn | grep l2auth
  ip link set "${DEVICE}" down
  ip link del "${L2AUTH_DEV}"
  exit 0
}
trap 'after_nc' EXIT

echo 'Waiting for a message from the attacker, press ^C to continue.'
echo 'The attackers message:'
nc -u -k -l -p 3000 || true
