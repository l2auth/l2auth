#!/bin/sh -e

VICTIM_IP='10.0.1.2'
VICTIM_MAC='c8:bc:c8:a4:f3:30'
DEVICE='eth0'
L2AUTH_DEV='l2a0'
IP='10.0.1.1'

RANDOM_KEY=A234567890123456789012345678901234567890123456789012345678901234

ip addr flush dev "${DEVICE}"
ip -6 addr flush dev "${DEVICE}"

modprobe l2auth

ip link add link "${DEVICE}" name "${L2AUTH_DEV}" type l2auth key "${RANDOM_KEY}"
ip addr add "${IP}/24" dev "${L2AUTH_DEV}"
ip link set "${DEVICE}" up
ip link set "${L2AUTH_DEV}" up

arp -s "${VICTIM_IP}" "${VICTIM_MAC}"

echo 'Perform the attack...'
echo 'Thou shalt not read this message!' | nc -w 0 -u "${VICTIM_IP}" 3000 || true

ip link set "${DEVICE}" down
ip link del "${L2AUTH_DEV}"
