#!/bin/sh -e

usage() {
	echo "Usage: $0 [L2AUTH]" >&2
	echo >&2
	echo " L2AUTH = name of the L2auth interface" >&2
	return 1
}

[ $# -eq 1 ] || usage

L2AUTH=$1

ip link del "${L2AUTH}"
