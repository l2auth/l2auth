#!/bin/sh -e

. ./_functions.sh

if [ $# -ne 2 ]
then
	echo "Usage: $0 {if1} {if2}" >&2
	exit 1
fi

IF1=${1:?}
IF2=${2:?}

PSK=00112233445566778899AABBCCDDEEFF00112233445566778899AABBCCDDEEFF

NETNS1_NAME=vnet1
NETNS2_NAME=vnet2

# Configure netns 1
if ! netns_exists "${NETNS1_NAME}"
then
	ensure_netns_exists "${NETNS1_NAME}"
	iface_put_netns "${IF1}" "${NETNS1_NAME}"
	iface_up "${IF1}" "${NETNS1_NAME}"
fi

# Configure netns 2
if ! netns_exists "${NETNS2_NAME}"
then
	ensure_netns_exists "${NETNS2_NAME}"
	iface_put_netns "${IF2}" "${NETNS2_NAME}"
	iface_up "${IF2}" "${NETNS2_NAME}"
fi

load_kmod l2auth || load_ko ../../src/l2auth/l2auth.ko

# Configure netns 1
if ! iface_in_netns l2a1 "${NETNS1_NAME}"
then
	_ ip -netns "${NETNS1_NAME}" link add link "${IF1}" l2a1 type l2auth key "${PSK}"
	_ ip -netns "${NETNS1_NAME}" addr add 172.16.0.1/24 dev l2a1
	_ ip -netns "${NETNS1_NAME}" link set dev l2a1 up
fi

# Configure netns 2
if ! iface_in_netns l2a2 "${NETNS2_NAME}"
then
	_ ip -netns "${NETNS2_NAME}" link add link "${IF2}" l2a2 type l2auth key "${PSK}"
	_ ip -netns "${NETNS2_NAME}" addr add 172.16.0.2/24 dev l2a2
	_ ip -netns "${NETNS2_NAME}" link set dev l2a2 up
fi
