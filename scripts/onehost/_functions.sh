#!/bin/sh -e

if [ ${_FUNCTIONS_LOADED_:-0} -lt 1 ]
then

_FUNCTIONS_LOADED_=1

_() {
	echo + $* >&2
	eval $*
}

netns_exists() {
	ip netns show | grep -q "^${1:?}"
}

iface_exists() {
	if test 1 -lt $#
	then
		# Test in netns
		netns_exists "$2" || return 1
		ip -netns "$2" link show dev "${1:?}" >/dev/null 2>&1
	else
		ip link show dev "${1:?}" >/dev/null 2>&1
	fi
}

ensure_netns_exists() {
	if ! netns_exists "${1:?}"
	then
		_ ip netns add "${1:?}"
	fi
}

iface_in_netns() {
	netns_exists "${2:?}" || return 1
	ip -netns "${2:?}" link show "${1:?}" >/dev/null 2>&1
}

iface_put_netns() {
	if ! (iface_exists "${1:?}" || iface_exists "${1:?}" "${2:?}")
	then
		echo "No such device ${1:?}" >&2
		return 1
	fi

	if ! iface_in_netns "${1:?}" "${2:?}"
	then
		ensure_netns_exists "${2:?}"
		_ ip link set "${1:?}" netns "${2:?}"
	fi
}

iface_up() {
	if test 0 -eq "$(ip -netns "${2:?}" link show "${1:?}" up 2>/dev/null | wc -l)"
	then
		_ ip -netns "${2:?}" link set dev "${1:?}" up
	fi
}

load_kmod() {
	if ! lsmod | grep -q "${1:?}"
	then
		modinfo "${1:?}" >/dev/null && _ modprobe "${1:?}"
	fi
}

load_ko() {
	if ! lsmod | grep -q "$(basename "${1:?}" .ko)"
	then
		if ! test -f "${1:?}"
		then
			echo "File ${1} does not exist" >&2
			return 2
		fi
		_ insmod "${1:?}"
	fi
}

unload_kmod() {
	lsmod | grep -q "${1:?}" && _ rmmod "${1:?}"
}

fi
