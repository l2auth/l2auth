#!/bin/sh -e

. ./_functions.sh

NETNS1_NAME=vnet1
NETNS2_NAME=vnet2

if iface_in_netns l2a1 "${NETNS1_NAME}"
then
	# Unconfigure l2a1
	_ ip -netns "${NETNS1_NAME}" link set l2a1 down
	_ ip -netns "${NETNS1_NAME}" link delete l2a1
fi

if iface_in_netns l2a2 "${NETNS2_NAME}"
then
	# Unconfigure l2a2
	_ ip -netns "${NETNS2_NAME}" link set l2a2 down
	_ ip -netns "${NETNS2_NAME}" link delete l2a2
fi

unload_kmod l2auth
