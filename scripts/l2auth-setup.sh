#!/bin/sh -e

KEY=${KEY:-00112233445566778899AABBCCDDEEFF00112233445566778899AABBCCDDEEFF}

usage() {
	echo "Usage: $0 [PHY] [L2AUTH] [CIDR]" >&2
	echo >&2
	echo " PHY = name of the physical interface to use" >&2
	echo " L2AUTH = name of the L2auth interface to create" >&2
	echo " CIDR = IP/prefix to assign to L2auth interface" >&2
	return 1
}

[ $# -eq 3 ] || usage

PHY=$1
L2AUTH=$2
CIDR=$3

modprobe l2auth
sysctl -q -w "net.ipv6.conf.${PHY}.autoconf"=0
ip address flush dev "${PHY}"
ip -6 address flush dev "${PHY}"
ip link add link "${PHY}" "${L2AUTH}" type l2auth key "${KEY}"
ip addr add "${CIDR}" dev "${L2AUTH}"
ip link set dev "${PHY}" up
ip link set dev "${L2AUTH}" up
