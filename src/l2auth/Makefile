# SPDX-License-Identifier: GPL-2.0
#
# Copyright (c) 2019 Dennis Camera <camerden@students.zhaw.ch>
#                    Raphael Ungricht <ungrirap@students.zhaw.ch>

ifeq ($(KERNELDIR),)
ifneq ($(realpath ../../linux),)
KERNELDIR ?= ../../linux
else
KERNELDIR ?= /lib/modules/$(shell uname -r)/build
endif
endif

ifeq ($(wildcard $(KERNELDIR)),)
$(error Cannot find Linux kernel sources!)
endif
KERNELDIR := $(abspath $(KERNELDIR))

DEPMOD ?= depmod

PWD := $(shell pwd -P)

obj-m := l2auth.o

DEBUG ?= 0

ifneq ($(DEBUG),0)
CFLAGS_l2auth.o += -DDEBUG
endif
ifneq ($(wildcard $(POSTPROC)),)
CFLAGS_l2auth.o +=-include '$(abspath $(POSTPROC))'
endif

V ?= 0
export V

ifeq ($(V),0)
MAKEFLAGS += --no-print-directory
endif

all: module

.PHONY: version.h
version.h:
	export GIT_CEILING_DIRECTORIES='$(realpath ../../..)' && \
	ver="#define L2AUTH_VERSION \"$$(git describe --always --tags --dirty 2>/dev/null)\"" && \
	[ "$$(cat version.h 2>/dev/null)" = "$$ver" ] || { \
		echo "$$ver" >version.h && \
		git update-index --assume-unchanged version.h; \
	}

.PHONY: dkms.conf
dkms.conf:
	export GIT_CEILING_DIRECTORIES='$(realpath ../../..)' && \
	ver="$$(git describe --always --tags --dirty 2>/dev/null)" && \
	[ "$$(. ./dkms.conf && echo "$$PACKAGE_VERSION")" = "$$ver" ] || { \
		sed -i "s/^PACKAGE_VERSION=.*$$/PACKAGE_VERSION=\"$$ver\"/" dkms.conf && \
		git update-index --assume-unchanged dkms.conf; \
	}


module: version.h
	$(MAKE) -C '$(KERNELDIR)' M='$(PWD)' V=$(V) modules

dkms-build:
	@$(MAKE) -C '$(PWD)' version.h dkms.conf
	dkms build '$(PWD)'

dkms-install:
	dkms install l2auth/$(shell . ./dkms.conf && echo $$PACKAGE_VERSION)

install:
	$(MAKE) -C '$(KERNELDIR)' M='$(PWD)' V=$(V) modules_install
	$(DEPMOD) -a

clean:
	$(MAKE) -C '$(KERNELDIR)' M='$(PWD)' V=$(V) clean

.PHONY: module install dkms-build dkms-install clean
