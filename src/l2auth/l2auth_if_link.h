#ifndef _UAPI_LINUX_L2AUTH_IF_LINK_H
#define _UAPI_LINUX_L2AUTH_IF_LINK_H

/* TODO: merge this file with if_link.h */


/* L2auth section  */
enum {
	IFLA_L2AUTH_UNSPEC,          /* dummy, does not work otherwise */
	IFLA_L2AUTH_INSTRUCTIONS,    /* flags that define what to do */
	IFLA_L2AUTH_KEY,             /* primary key */
	IFLA_L2AUTH_BACKUP_KEY,      /* secondary key for smooth key rotation */
	__IFLA_L2AUTH_MAX,
};

#define IFLA_L2AUTH_MAX (__IFLA_L2AUTH_MAX - 1)


struct ilfa_l2auth_instructions {
	__u32 set_key : 1;
	__u32 del_key : 1;
	__u32 set_backup_key : 1;
	__u32 del_backup_key : 1;
} __attribute__((packed));


#endif /* _UAPI_LINUX_L2AUTH_IF_LINK_H */
