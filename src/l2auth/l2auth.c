/*
 * l2auth/l2auth.c - l2auth device
 *
 * Copyright (c) 2019 Dennis Camera <camerden@students.zhaw.ch>
 *                    Raphael Ungricht <ungrirap@students.zhaw.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <linux/skbuff.h>
#include <linux/etherdevice.h>
#include <net/rtnetlink.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <crypto/hash.h>

#include "l2auth_if_link.h"  /* TODO: Remove when this module is merged into mainline */
#include "version.h"

#define MODULE_NAME "l2auth"
#undef pr_fmt
#define pr_fmt(fmt) MODULE_NAME ": " fmt

#define l2auth_debug(fmt, ...) \
	pr_debug("%s:%u: " fmt, __FUNCTION__, __LINE__, ##__VA_ARGS__)

#define l2auth_pr_debug_dev(dev, fmt, ...) \
	l2auth_debug("[dev=%s] " fmt, dev->name, ##__VA_ARGS__)

#define l2auth_pr_info_dev(dev, fmt, ...) \
	pr_info("[dev=%s] " fmt, dev->name, ##__VA_ARGS__)

#define l2auth_pr_warn_dev(dev, fmt, ...) \
	pr_warn("[dev=%s] " fmt, dev->name, ##__VA_ARGS__)

#define l2auth_pr_err_dev(dev, fmt, ...) \
	pr_err("[dev=%s] " fmt, dev->name, ##__VA_ARGS__)

#define l2auth_skb_debug(skb) \
	l2auth_pr_debug_dev(skb->dev, "[sk_buff] head=%p, data=%p, tail=%p, end=%p len=%u\n", \
	skb->head, \
	skb->data, \
	skb_tail_pointer(skb), \
	skb_end_pointer(skb), \
	skb->len)

#define ETH_P_L2AUTH 0x88E6  /* L2auth ether type */
#define HMAC_ALGO "sha256"
#define HMAC_LEN 32
#define L2AUTH_KEY_LEN 64
#define L2AUTH_KEY_LEN_MIN 32


struct l2auth_eth_header {
	struct ethhdr eth;
	__be16 h_real_proto;
} __attribute__((packed));

#define L2AUTH_HEADER_EXTRA_LEN ((unsigned int)sizeof(struct l2auth_eth_header) \
                                 - (unsigned int)sizeof(struct ethhdr))


struct l2auth_eth_trailer {
	unsigned char hmac[HMAC_LEN];
} __attribute__((packed));

#define L2AUTH_TRAILER_EXTRA_LEN ((unsigned int)sizeof(struct l2auth_eth_trailer))

#define L2AUTH_EXTRA_LEN (L2AUTH_HEADER_EXTRA_LEN + L2AUTH_TRAILER_EXTRA_LEN)

#define l2auth_skb_get_data(skb) (skb_mac_header(skb) + sizeof(struct l2auth_eth_header))
#define l2auth_skb_get_data_len(skb) ((skb_tail_pointer(skb) - skb_mac_header(skb)) - sizeof(struct l2auth_eth_trailer) - sizeof(struct l2auth_eth_header))

#define l2auth_skb_get_header(skb) ((struct l2auth_eth_header *)skb_mac_header(skb))
#define l2auth_skb_get_trailer(skb) ((struct l2auth_eth_trailer *)(skb_tail_pointer(skb) - L2AUTH_TRAILER_EXTRA_LEN))

/**
 * struct l2auth_dev - private device data
 * @real_dev: underlying netdevice
 */
struct l2auth_dev {
	struct net_device *real_dev;
	struct crypto_shash *tfm;
	struct crypto_shash *tfm_backup;
	struct {
		unsigned has_key : 1;
		unsigned has_backup_key : 1;
	};
};

struct l2auth_rxhndlr_data {
	/* empty */
};



static struct l2auth_dev *l2auth_priv(const struct net_device *dev)
{
	return (struct l2auth_dev *)netdev_priv(dev);
}


#define L2AUTH_FEATURES (NETIF_F_SG | NETIF_F_HIGHDMA | NETIF_F_FRAGLIST)

static int l2auth_dev_init(struct net_device *dev)
{
	struct l2auth_dev *l2auth = l2auth_priv(dev);
	struct net_device *real_dev = l2auth->real_dev;

	l2auth_debug("called with dev = %s(%p)\n", dev->name, dev);

	dev->tstats = netdev_alloc_pcpu_stats(struct pcpu_sw_netstats);
	if (!dev->tstats)
		return -ENOMEM;

	dev->features = real_dev->features & L2AUTH_FEATURES;
	dev->features |= NETIF_F_GSO_SOFTWARE;

	dev->needed_headroom = real_dev->needed_headroom + L2AUTH_HEADER_EXTRA_LEN;
	dev->needed_tailroom = real_dev->needed_tailroom + L2AUTH_TRAILER_EXTRA_LEN;

	/* Inherit parent's MAC address if unset */
	if (is_zero_ether_addr(dev->dev_addr))
		eth_hw_addr_inherit(dev, real_dev);

	/* Inherit parent's broadcast address if unset */
	if (is_zero_ether_addr(dev->broadcast))
		memcpy(dev->broadcast, real_dev->broadcast, dev->addr_len);

	return 0;
}


static void l2auth_dev_uninit(struct net_device *dev)
{
	free_percpu(dev->tstats);
}


static int l2auth_dev_open(struct net_device *dev)
{
	struct l2auth_dev *l2auth = l2auth_priv(dev);
	struct net_device *real_dev = l2auth->real_dev;
	int err;

	l2auth_debug("called with dev = %s(%p)\n", dev->name, dev);

	/* Check if the parent is up */
	if (!(real_dev->flags & IFF_UP))
		return -ENETDOWN;

	if (0 > (err = dev_uc_add(real_dev, dev->dev_addr)))
		return err;

	if (dev->flags & IFF_ALLMULTI) {
		if (0 > (err = dev_set_allmulti(real_dev, 1)))
			goto del_unicast;
	}

	if (dev->flags & IFF_PROMISC) {
		if (0 > (err = dev_set_promiscuity(real_dev, 1)))
			goto clear_allmulti;
	}

	if (netif_carrier_ok(real_dev))
		netif_carrier_on(dev);

	return 0;

  clear_allmulti:
	if (dev->flags & IFF_ALLMULTI)
		dev_set_allmulti(real_dev, -1);

  del_unicast:
	dev_uc_del(real_dev, dev->dev_addr);
	netif_carrier_off(dev);

	return err;
}


static int l2auth_dev_stop(struct net_device *dev)
{
	struct l2auth_dev *l2auth = l2auth_priv(dev);
	struct net_device *real_dev = l2auth->real_dev;

	netif_carrier_off(dev);

	dev_mc_unsync(real_dev, dev);
	dev_uc_unsync(real_dev, dev);

	if (dev->flags & IFF_ALLMULTI)
		dev_set_allmulti(real_dev, -1);

	if (dev->flags & IFF_PROMISC)
		dev_set_promiscuity(real_dev, -1);

	dev_uc_del(real_dev, dev->dev_addr);

	return 0;
}


static int l2auth_change_mtu(struct net_device *dev, int new_mtu)
{
	struct l2auth_dev *l2auth = l2auth_priv(dev);

	/* Check if the MTU is small enough, so that after we added our overhead the
	 * data still fits into the underlying device's MTU */
	if ((l2auth->real_dev->mtu - L2AUTH_EXTRA_LEN) < new_mtu)
		return -ERANGE;

	dev->mtu = new_mtu;

	return 0;
}


static int l2auth_calculate_hmac(const struct sk_buff *skb, struct crypto_shash *tfm, size_t len, unsigned char *res)
{
	SHASH_DESC_ON_STACK(sdesc, tfm);
	void *skb_buf = skb_mac_header(skb);
	int err;

	sdesc->tfm = tfm;
	sdesc->flags = 0x0;

	err = crypto_shash_digest(sdesc, skb_buf, len, res);

#ifdef L2AUTH_HMAC_POSTPROC
	if (!err) {
		/* Run HMAC post processing */
		L2AUTH_HMAC_POSTPROC(res, HMAC_LEN);
	}
#endif

	return err;
}


static int l2auth_check_auth_skb(const struct sk_buff *skb)
{
	struct l2auth_dev *l2auth = l2auth_priv(skb->dev);
	unsigned char *hmac_should, hmac_is[HMAC_LEN];
	size_t skb_hash_len = (skb->len + (skb->data - skb_mac_header(skb)) - L2AUTH_TRAILER_EXTRA_LEN);

	hmac_should = l2auth_skb_get_trailer(skb)->hmac;

	/* Check authentication using primary key */
	if (l2auth->has_key) {
		if (l2auth_calculate_hmac(skb, l2auth->tfm, skb_hash_len, hmac_is))
			return -1;

		if (!memcmp(hmac_should, hmac_is, HMAC_LEN))
			return 0;
	}

	/* Check authentication using backup key */
	if (l2auth->has_backup_key) {
		pr_debug("Checking HMAC with backup key...");
		if (l2auth_calculate_hmac(skb, l2auth->tfm_backup, skb_hash_len, hmac_is))
			return -1;

		if (!memcmp(hmac_should, hmac_is, HMAC_LEN)) {
			l2auth_pr_debug_dev(
				skb->dev, "Received frame authenticated with backup key. "
				"Are you performing a key rotation?");
			return 0;
		}
	}

	l2auth_pr_err_dev(
		skb->dev, "Received frame authenticated with unknown key!\n");
	return -1;
}

static struct sk_buff *l2auth_authenticate_skb(
	struct sk_buff *skb, struct net_device *dev)
{
	size_t hash_len;
	struct ethhdr *eth_head;
	struct l2auth_eth_header *l2a_head;
	struct l2auth_eth_trailer *l2a_tail;
	struct l2auth_dev *l2auth = l2auth_priv(skb->dev);

	l2auth_pr_debug_dev(dev, "authenticating skb %p\n", skb);

	if (!l2auth->has_key) {
		l2auth_pr_err_dev(dev, "Device has no key set. Cannot send frames! "
		                  "Please set a key first.\n");
		return ERR_PTR(-EINVAL);
	}

	if (unlikely(skb_headroom(skb) < L2AUTH_HEADER_EXTRA_LEN
	             || skb_tailroom(skb) < L2AUTH_TRAILER_EXTRA_LEN)) {
		/* Make space in SKB and use the expanded SKB instead */
		struct sk_buff *nskb = skb_copy_expand(
			skb, L2AUTH_HEADER_EXTRA_LEN, L2AUTH_TRAILER_EXTRA_LEN,
			GFP_ATOMIC);
		if (likely(nskb)) {
			consume_skb(skb);
			skb = nskb;
		} else {
			kfree_skb(skb);
			return ERR_PTR(-ENOMEM);
		}
	} else {
		skb = skb_unshare(skb, GFP_ATOMIC);
		if (!skb)
			return ERR_PTR(-ENOMEM);
	}

	/* Make space for l2auth header */
	eth_head = eth_hdr(skb);
	l2a_head = (struct l2auth_eth_header *)skb_push(skb, L2AUTH_HEADER_EXTRA_LEN);

	/* Move real ethernet header to front of packet again) */
	memmove(l2a_head, eth_head, sizeof(struct ethhdr));
	eth_head = (struct ethhdr *)l2a_head;
	skb_reset_mac_header(skb);

	l2auth_pr_debug_dev(dev, "storing real ethertype 0x%x\n",
	                    ntohs(eth_head->h_proto));
	l2a_head->h_real_proto = eth_head->h_proto;

	/* Mark as an l2auth frame */
	skb->protocol = eth_head->h_proto = htons(ETH_P_L2AUTH);

	/* Extend skb to make space for l2auth authentication (trailer) */
	hash_len = skb->len;
	l2a_tail = (struct l2auth_eth_trailer *)skb_put(skb, L2AUTH_TRAILER_EXTRA_LEN);

	/* As the last step, calculate the HMAC. This must be done after extending
	 * the header, because the header fields also get authenticated. */

	l2auth_calculate_hmac(skb, l2auth->tfm, hash_len, l2auth_skb_get_trailer(skb)->hmac);

	return skb;
}


static netdev_tx_t l2auth_start_xmit(struct sk_buff *skb, struct net_device *dev)
{
	struct pcpu_sw_netstats *tstats = this_cpu_ptr(dev->tstats);
	struct l2auth_dev *l2auth = l2auth_priv(dev);
	int ret, raw_len = skb->len;

	skb = l2auth_authenticate_skb(skb, dev);
	if (IS_ERR(skb)) {
		l2auth_pr_err_dev(dev, "Authentication failed\n");
		return NETDEV_TX_OK;
	}

	l2auth_pr_debug_dev(dev, "skb = %p (ethertype = 0x%x/0x%x, len = %u, dest = %pM)\n",
	                    skb, ntohs(skb->protocol),
	                    ntohs(eth_hdr(skb)->h_proto),
	                    skb->len, eth_hdr(skb)->h_dest);

	/* Send skb on real_dev */
	skb->dev = l2auth->real_dev;  /* send out on real_dev */

	skb_tx_timestamp(skb);

	ret = dev_queue_xmit(skb);

	if (likely(ret == NET_XMIT_SUCCESS || ret == NET_XMIT_CN)) {
		/* Count transmitted packet */
		u64_stats_update_begin(&tstats->syncp);
		tstats->tx_packets++;
		tstats->tx_bytes += raw_len;
		u64_stats_update_end(&tstats->syncp);
	} else {
		/* Packet was dropped because of congestion */
		atomic_long_inc(&(dev->tx_dropped));  /* should not use this counter... */
		return NETDEV_TX_BUSY;
	}

	return NETDEV_TX_OK;
}


static int l2auth_set_mac_address(struct net_device *dev, void *ptr)
{
	struct l2auth_dev *l2auth = l2auth_priv(dev);
	struct net_device *real_dev = l2auth->real_dev;
	struct sockaddr *addr = ptr;
	int err;

	if (!is_valid_ether_addr(addr->sa_data))
		return -EADDRNOTAVAIL;

	if ((dev->flags & IFF_UP)) {
		if (0 > (err = dev_uc_add(real_dev, addr->sa_data)))
			return err;

		dev_uc_del(real_dev, dev->dev_addr);
	}

	ether_addr_copy(dev->dev_addr, addr->sa_data);
	return 0;
}


static void l2auth_get_stats64(struct net_device *dev,
                               struct rtnl_link_stats64 *s)
{
	int cpu;

	if (!dev->tstats)
		return;

	for_each_possible_cpu(cpu) {
		struct pcpu_sw_netstats *stats, tmp;
		int start;

		stats = per_cpu_ptr(dev->tstats, cpu);
		do {
			start = u64_stats_fetch_begin_irq(&stats->syncp);
			tmp.rx_packets = stats->rx_packets;
			tmp.rx_bytes = stats->rx_bytes;
			tmp.tx_packets = stats->tx_packets;
			tmp.tx_bytes = stats->tx_bytes;
		} while (u64_stats_fetch_retry_irq(&stats->syncp, start));

		s->rx_packets += tmp.rx_packets;
		s->rx_bytes += tmp.rx_bytes;
		s->tx_packets += tmp.tx_packets;
		s->tx_bytes += tmp.tx_bytes;
	}

	s->rx_dropped = dev->stats.rx_dropped;
	s->tx_dropped = dev->stats.tx_dropped;
}


static void l2auth_dev_set_rx_mode(struct net_device *dev)
{
	struct l2auth_dev *l2auth = l2auth_priv(dev);
	struct net_device *real_dev = l2auth->real_dev;

	dev_mc_sync(real_dev, dev);
	dev_uc_sync(real_dev, dev);
}


static void l2auth_dev_change_rx_flags(struct net_device *dev, int change)
{
	struct l2auth_dev *l2auth = l2auth_priv(dev);
	struct net_device *real_dev = l2auth->real_dev;

	if (!(dev->flags & IFF_UP))
		return;

	if ((change & IFF_ALLMULTI))
		dev_set_allmulti(real_dev, ((dev->flags & IFF_ALLMULTI) ? 1 : -1));

	if (change & IFF_PROMISC)
		dev_set_promiscuity(
			real_dev, ((dev->flags & IFF_PROMISC) ? 1 : -1));
}


static const struct net_device_ops l2auth_netdev_ops = {
	.ndo_init = l2auth_dev_init,
	.ndo_uninit = l2auth_dev_uninit,
	.ndo_open = l2auth_dev_open,
	.ndo_stop = l2auth_dev_stop,
	.ndo_change_mtu = l2auth_change_mtu,
	.ndo_set_rx_mode = l2auth_dev_set_rx_mode,
	.ndo_change_rx_flags = l2auth_dev_change_rx_flags,
	.ndo_start_xmit = l2auth_start_xmit,
	.ndo_set_mac_address = l2auth_set_mac_address,
	.ndo_get_stats64 = l2auth_get_stats64,
};


static void l2auth_crypto_uninit(struct l2auth_dev *l2auth)
{
	/* Free HMAC transform */
	if (l2auth->tfm) {
		crypto_free_shash(l2auth->tfm);
		l2auth->tfm = NULL;
	}

	if (l2auth->tfm_backup) {
		crypto_free_shash(l2auth->tfm_backup);
		l2auth->tfm_backup = NULL;
	}
}


static void l2auth_free_netdev(struct net_device *dev)
{
	struct l2auth_dev *l2auth = l2auth_priv(dev);
	struct net_device *real_dev = l2auth->real_dev;

	/* free_percpu(l2auth->tstats); */

	l2auth_crypto_uninit(l2auth);

	/* Decrement the ref counter for the real device. */
	dev_put(real_dev);
}


static const struct device_type l2auth_type = {
	.name = "l2auth",
};


static void l2auth_setup(struct net_device *dev)
{
	/* Initialize a basic ethernet-device */
	ether_setup(dev);

	/* Initialise the device structure. */
	dev->netdev_ops = &l2auth_netdev_ops;

	/* The device can run without qdisc attached */
	dev->priv_flags |= IFF_NO_QUEUE;

	/* Add a destructor, that is called when all references are gone */
	dev->priv_destructor = l2auth_free_netdev;

	/* Defines the new type for the device */
	SET_NETDEV_DEVTYPE(dev, &l2auth_type);

	/* Adjust the limits of the MTU */
	dev->max_mtu -= L2AUTH_EXTRA_LEN;
}


static int l2auth_crypto_init(struct net_device *dev)
{
	struct l2auth_dev *l2auth = l2auth_priv(dev);
	struct crypto_shash *tfm;

	/* Allocate HMAC transform for generating packet authentication */
	tfm = crypto_alloc_shash("hmac(" HMAC_ALGO ")", 0, 0);
	if (IS_ERR(tfm)) {
		/* FIXME */
		net_info_ratelimited("failed to load transform for hmac(" HMAC_ALGO "): %ld\n",
		                     PTR_ERR(tfm));
		return -ENOSYS;
	}
	l2auth->tfm = tfm;

	/* Allocate a secondary HMAC transform for generating packet authentication
	 * using the backup key */
	tfm = crypto_alloc_shash("hmac(" HMAC_ALGO ")", 0, 0);
	if (IS_ERR(tfm)) {
		/* FIXME */
		net_info_ratelimited("failed to load transform for hmac(" HMAC_ALGO "): %ld\n",
		                     PTR_ERR(tfm));
		return -ENOSYS;
	}
	l2auth->tfm_backup = tfm;

	return 0;
}


/**
 * This function handles an skb received by the underlying device.
 * Its purpose is to divert it to the upper l2auth device.
 */
static rx_handler_result_t l2auth_rx_handler_under(struct sk_buff **pskb)
{
	struct sk_buff *skb = *pskb;
	struct list_head *dev_upper_list;
	struct net_device *upper = NULL;

	if (!skb) {
		pr_info("Empty sk_buff received on underlying device\n");
		return RX_HANDLER_CONSUMED;
	}

	l2auth_pr_debug_dev(skb->dev, "packet received on underlying device\n");

	rcu_read_lock();
	dev_upper_list = &(skb->dev->adj_list.upper);
	while ((upper = netdev_upper_get_next_dev_rcu(skb->dev, &dev_upper_list))) {
		if (likely(upper->dev.type == &l2auth_type))
			break;
	}
	rcu_read_unlock();

	if (upper) {
		skb->dev = upper;
		l2auth_debug("redirecting skb %p to dev %s\n", skb, upper->name);
		return RX_HANDLER_ANOTHER;
	}

	return RX_HANDLER_CONSUMED;
}

static rx_handler_result_t l2auth_rx_handler(struct sk_buff **pskb)
{
	/* include/linux/skbuff.h:665 */
	struct sk_buff *skb = *pskb;
	struct pcpu_sw_netstats *tstats = this_cpu_ptr(skb->dev->tstats);
	struct l2auth_eth_header *l2a_head = l2auth_skb_get_header(skb);

	if (!skb) {
		pr_info("Empty sk_buff received!");
		return RX_HANDLER_CONSUMED;
	}

	l2auth_pr_debug_dev(skb->dev, "skb = %p (linear = %u)\n", skb, skb_is_nonlinear(skb));
	l2auth_skb_debug(skb);

	/* Check packet ether type */
	if (ntohs(l2a_head->eth.h_proto) != ETH_P_L2AUTH) {
		pr_warning("Received non-l2auth skb (ethertype = 0x%x) on dev %s. "
		           "Dropping!\n", ntohs(l2a_head->eth.h_proto), skb->dev->name);
		goto drop;
	}

	skb_linearize(skb);

	/* Check packet authentication */
	if (l2auth_check_auth_skb(skb)) {
		pr_warning("HMAC of received skb (ethertype = 0x%x) on dev %s is "
		           "incorrect. Dropping!\n", ntohs(l2a_head->eth.h_proto),
		           skb->dev->name);
		goto drop;
	}

	l2auth_skb_debug(skb);

	/* Restore real ether type */
	l2auth_pr_debug_dev(skb->dev, "Restoring real ethertype 0x%x\n",
	                    ntohs(l2a_head->h_real_proto));
	skb->protocol = l2a_head->eth.h_proto = l2a_head->h_real_proto;

	/* Restore original ethernet header (drop l2auth extra) */
	memmove(((uint8_t *)l2a_head) + L2AUTH_HEADER_EXTRA_LEN, l2a_head,
	        sizeof(struct ethhdr));
	skb_pull(skb, L2AUTH_HEADER_EXTRA_LEN);

	skb_reset_network_header(skb);
	if (!skb_transport_header_was_set(skb))
		skb_reset_transport_header(skb);
	skb_reset_mac_len(skb);

	pskb_trim_unique(skb, skb->len - L2AUTH_TRAILER_EXTRA_LEN);

	tstats->rx_packets++;
	tstats->rx_bytes += skb->len;

	return RX_HANDLER_PASS;

drop:
	atomic_long_inc(&(skb->dev->rx_dropped));  /* should not use this counter... */
	kfree_skb(skb);
	*pskb = NULL;
	return RX_HANDLER_CONSUMED;
}


static int l2auth_register_rx_handlers(
	struct net_device *real_dev, struct net_device *dev)
{
	struct l2auth_rxhndlr_data *rxd = rtnl_dereference(dev->rx_handler_data),
		*rxd_under = rtnl_dereference(real_dev->rx_handler_data);
	int err;

	if (!rxd) {
		rxd = kmalloc(sizeof(*rxd), GFP_KERNEL);
		if (!rxd)
			return -ENOMEM;

		err = netdev_rx_handler_register(dev, l2auth_rx_handler, rxd);
		if (err < 0) {
			kfree(rxd);
			return err;
		}
	}

	if (!rxd_under) {
		/* underlying device has no rx_handler registered, yet */

		rxd_under = kmalloc(sizeof(*rxd_under), GFP_KERNEL);
		if (!rxd_under)
			return -ENOMEM;

		err = netdev_rx_handler_register(
			real_dev, l2auth_rx_handler_under, rxd_under);
		if (err < 0) {
			kfree(rxd_under);
			return err;
		}
	}

	return 0;
}

static int l2auth_unregister_rx_handlers(
	struct net_device *real_dev, struct net_device *dev)
{
	netdev_rx_handler_unregister(real_dev);
	netdev_rx_handler_unregister(dev);
	return 0;
}


static int l2auth_link_setprop(struct net_device *dev, struct nlattr *data[])
{
	int ret = 0;
	struct l2auth_dev *l2auth = l2auth_priv(dev);

	struct ilfa_l2auth_instructions *instr;

	if (!data) {
		pr_debug("setprop called without data.\n");
		return 0;
	}

	if (data[IFLA_L2AUTH_INSTRUCTIONS]) {
		instr = nla_data(data[IFLA_L2AUTH_INSTRUCTIONS]);
	} else {
		l2auth_pr_warn_dev(dev, "setprop called without instructions. Ignoring request!\n");
		return 0;
	}

	if ((instr->set_key && !instr->del_key) && data[IFLA_L2AUTH_KEY]) {
		const __u8 *key = nla_data(data[IFLA_L2AUTH_KEY]);
		int len = nla_len(data[IFLA_L2AUTH_KEY]);

		/* Check length of key */
		if (len > L2AUTH_KEY_LEN || len < L2AUTH_KEY_LEN_MIN)
			return -EINVAL;

		if ((ret = crypto_shash_setkey(l2auth->tfm, key, len)))
			return ret;

		l2auth->has_key = 1;
		l2auth_pr_info_dev(dev, "The primary key has been changed\n");
	} else {
		l2auth_pr_debug_dev(dev, "setprop: primary key has not been modified\n");
	}

	if (instr->del_key) {
		__u8 empty_key[8] = {0};
		crypto_shash_setkey(l2auth->tfm, empty_key, sizeof(empty_key));
		l2auth->has_key = 0;
		l2auth_pr_info_dev(dev, "The primary key has been cleared\n");
	}

	if ((instr->set_backup_key && !instr->del_backup_key)
	    && data[IFLA_L2AUTH_BACKUP_KEY]) {
		const __u8 *key = nla_data(data[IFLA_L2AUTH_BACKUP_KEY]);
		int len = nla_len(data[IFLA_L2AUTH_BACKUP_KEY]);

		/* Check length of key */
		if (len > L2AUTH_KEY_LEN || len < L2AUTH_KEY_LEN_MIN)
			return -EINVAL;

		if ((ret = crypto_shash_setkey(l2auth->tfm_backup, key, len)))
			return ret;

		l2auth->has_backup_key = 1;
		l2auth_pr_info_dev(dev, "The backup key has been changed\n");
	} else {
		l2auth_pr_debug_dev(dev, "setprop: backup key has not been modified\n");
	}

	if (instr->del_backup_key) {
		__u8 empty_key[8] = {0};
		crypto_shash_setkey(l2auth->tfm_backup, empty_key, sizeof(empty_key));
		l2auth->has_backup_key = 0;
		l2auth_pr_info_dev(dev, "The backup key has been cleared\n");
	}

	return 0;
}

static int l2auth_newlink(struct net *net, struct net_device *dev,
                          struct nlattr *tb[], struct nlattr *data[],
                          struct netlink_ext_ack *extack)
{
	struct l2auth_dev *l2auth = l2auth_priv(dev);
	struct net_device *real_dev;
	rx_handler_func_t *rx_handler;
	int err;

	/* initialize everything to zero, to determine if a pointer was allocated,
	   and if it needs to be freed */
	memset(l2auth, 0, sizeof(struct l2auth_dev));

	/* The real device is missing */
	if (!tb[IFLA_LINK])
		return -ENODEV;

	if (!(real_dev = __dev_get_by_index(net, nla_get_u32(tb[IFLA_LINK]))))
		return -ENODEV;

	/* dev->priv_flags |= IFF_L2AUTH; */
	l2auth->real_dev = real_dev;

	dev->mtu = (real_dev->mtu - L2AUTH_EXTRA_LEN);

	rx_handler = rtnl_dereference(real_dev->rx_handler);
	if (rx_handler && rx_handler != l2auth_rx_handler) {
		/* The underlying interface already has a rx_handler registered -> error */
		return -EBUSY;
	}

	if (0 > (err = register_netdevice(dev)))
		return err;

	dev_hold(real_dev);
	dev->needs_free_netdev = true;

	if (0 > (err = netdev_upper_dev_link(real_dev, dev, extack)))
		goto unregister;

	if ((err = l2auth_crypto_init(dev)))
		goto unlink;

	if ((err = l2auth_link_setprop(dev, data)))
		goto del_dev;

	if (0 > (err = l2auth_register_rx_handlers(real_dev, dev)))
		goto del_dev;

	l2auth_pr_info_dev(dev, "link established successfully");
	return 0;

del_dev:
	l2auth_crypto_uninit(l2auth);

unlink:
	/* Unlink dev from real_dev */
	netdev_upper_dev_unlink(real_dev, dev);

unregister:
	unregister_netdevice(dev);

	return err;
}


static int l2auth_changelink(
	struct net_device *dev, struct nlattr *tb[], struct nlattr *data[],
	struct netlink_ext_ack *extack)
{
	int err = l2auth_link_setprop(dev, data);
	if (!err)
		l2auth_pr_info_dev(dev, "link changed successfully\n");
	return err;
}


static void l2auth_dellink(struct net_device *dev, struct list_head *head)
{
	struct l2auth_dev *l2auth = l2auth_priv(dev);
	struct net_device *real_dev = l2auth->real_dev;

	unregister_netdevice_queue(dev, head);

	/* unregister all the rx_handlers */
	l2auth_unregister_rx_handlers(real_dev, dev);

	/* Unlink the real device from the l2auth device. */
	netdev_upper_dev_unlink(real_dev, dev);
	/* Decrement the ref counter for the real device. */
	dev_put(real_dev);
}


static int l2auth_validate_nlattr(
	struct nlattr *tb[], struct nlattr *data[], struct netlink_ext_ack *extack)
{
	if (tb[IFLA_ADDRESS]) {
		if (nla_len(tb[IFLA_ADDRESS]) != ETH_ALEN)
			return -EINVAL;

		if (!is_valid_ether_addr(nla_data(tb[IFLA_ADDRESS])))
			return -EADDRNOTAVAIL;
	}

	if (!data)
		return 0;

	if (data[IFLA_L2AUTH_KEY]) {
		int len = nla_len(data[IFLA_L2AUTH_KEY]);

		if (len > L2AUTH_KEY_LEN || len < L2AUTH_KEY_LEN_MIN)
			return -EINVAL;
	}

	if (data[IFLA_L2AUTH_BACKUP_KEY]) {
		int len = nla_len(data[IFLA_L2AUTH_BACKUP_KEY]);

		if (len > L2AUTH_KEY_LEN || len < L2AUTH_KEY_LEN_MIN)
			return -EINVAL;
	}

	return 0;
}


static const struct nla_policy l2auth_nla_policy[IFLA_L2AUTH_MAX + 1] = {
	[IFLA_L2AUTH_INSTRUCTIONS] = { .len = sizeof(struct ilfa_l2auth_instructions) },
	[IFLA_L2AUTH_KEY] = { .type = NLA_BINARY, .len = L2AUTH_KEY_LEN },
	[IFLA_L2AUTH_BACKUP_KEY] = { .type = NLA_BINARY, .len = L2AUTH_KEY_LEN },
};

static size_t l2auth_nla_get_size(const struct net_device *dev)
{
	return
		/* IFLA_L2AUTH_KEY */
		nla_total_size(L2AUTH_KEY_LEN) +
		/* IFLA_L2AUTH_INSTRUCTIONS */
		nla_total_size(sizeof(struct ilfa_l2auth_instructions)) +
		/* IFLA_L2AUTH_BACKUP_KEY */
		nla_total_size(L2AUTH_KEY_LEN);
}


/**
 *	struct l2auth_link_ops - l2auth rtnetlink link operations
 *
 *	@kind: Identifier
 *	@maxtype: Highest device specific netlink attribute number
 *	@policy: Netlink policy for device specific attribute validation
 *	@validate: Optional validation function for netlink/changelink parameters
 *	@priv_size: sizeof net_device private space
 *	@setup: net_device setup function
 *	@newlink: Function for configuring and registering a new device
 *	@changelink: Function for changing parameters of an existing device
 *	@dellink: Function to remove a device
 *	@get_size: Function to calculate required room for dumping device
 *	           specific netlink attributes
 *	@fill_info: Function to dump device specific netlink attributes
 *	@get_xstats_size: Function to calculate required room for dumping device
 *	                  specific statistics
 *	@fill_xstats: Function to dump device specific statistics
 *	@get_num_tx_queues: Function to determine number of transmit queues
 *	                    to create when creating a new device.
 *	@get_num_rx_queues: Function to determine number of receive queues
 *	                    to create when creating a new device.
 *	@get_link_net: Function to get the i/o netns of the device
 *	@get_linkxstats_size: Function to calculate the required room for
 *	                      dumping device-specific extended link stats
 *	@fill_linkxstats: Function to dump device-specific extended link stats
 */
static struct rtnl_link_ops l2auth_link_ops __read_mostly = {
	.kind = "l2auth",
	.priv_size = sizeof(struct l2auth_dev),
	.setup = l2auth_setup,
	.validate = l2auth_validate_nlattr,
	.newlink = l2auth_newlink,
	.changelink = l2auth_changelink,
	.dellink = l2auth_dellink,

	/* rtnl */
	.maxtype = IFLA_L2AUTH_MAX,
	.policy = l2auth_nla_policy,
	.get_size = l2auth_nla_get_size,
};



/**
 * Module initialiser
 *
 * @return 0 on success, some error code otherwise.
 */
static int __init l2auth_init(void)
{
	pr_info("Authenticated layer 2 pseudo network driver\n");

	return rtnl_link_register(&l2auth_link_ops);
}


/**
 * Module destructor
 */
static void __exit l2auth_exit(void)
{
	rtnl_link_unregister(&l2auth_link_ops);
	rcu_barrier();
}


/*******************************************************************************
 * Module metadata
 */
module_init(l2auth_init);
module_exit(l2auth_exit);

MODULE_DESCRIPTION("Authenticated Layer 2 pseudo network device");
MODULE_LICENSE("GPL");
MODULE_VERSION(L2AUTH_VERSION);

MODULE_ALIAS_RTNL_LINK(MODULE_NAME);
