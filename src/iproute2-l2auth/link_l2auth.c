/*
 * link_l2auth.c
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version
 * 2 of the License, or (at your option) any later version.
 *
 * Authors: Raphael Ungricht <ungrirap@students.zhaw.ch>
 *          Dennis Camera <camerden@students.zhaw.ch>

 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <linux/genetlink.h>
#include <linux/if_ether.h>
#include <l2auth_if_link.h>

#include "utils.h"
#include "ip_common.h"


#define DRV_NAME "l2auth"
#define L2AUTH_BUFLEN 1024

#define KEY_LEN_MIN 32
#define KEY_LEN_MAX 64

static void print_usage(FILE *file)
{
	fprintf(file,
		"Usage: ... " DRV_NAME "\n"
		"                      [key { SECRET (32 byte hex) } ]\n"
		"                      [backup { SECRET (32 byte hex) } ]\n"
		"                      [del-backup]\n"
		"\n"
		"\n"
		"Example usage:\n"
		"    adding a new link\n"
		"        ip link add link {underlying link} {link name} type " DRV_NAME " key 00112233445566778899AABBCCDDEEFF00112233445566778899AABBCCDDEEFF\n"
		"    changing the link\n"
		"        ip link set dev {link name} type " DRV_NAME " key 00112233445566778899AABBCCDDEEFF00112233445566778899AABBCCDDEEFF\n"
		"\n"
		"key { SECRET (32 byte in hex format) }\n"
		"    This is the primary key that is always used to sign outgoing datagrams\n"
		"    and it is the primary key that validates the incoming datagram.\n"
		"\n"
		"backup { SECRET (32 byte in hex format) }\n"
		"    This is the secondary key. It is used as a fallback mechanism in case \n"
		"    the validation with the primary key failed.\n"
		"    This feature is important to support a smooth transition when\n"
		"    changing keys in a net.\n"
		"del-backup\n"
		"    This removes the backup-key\n"
	);
}

static void addattr_key(char **argv, int ifla, struct nlmsghdr *n)
{
	/* 4 Extra cells to determine if the given key is longer than KEY_LEN_MAX */
	__u8 key[KEY_LEN_MAX + 4];
	int len = 0;

	if (!hexstring_a2n(*argv, key, KEY_LEN_MAX + 4, &len))
		invarg("expected SECRET after \"key\"", "SECRET");
	if (len < KEY_LEN_MIN)
		invarg("The key is too short!", "SECRET");
	if (len > KEY_LEN_MAX)
		invarg("The key is too long!", "SECRET");

	addattr_l(n, L2AUTH_BUFLEN, ifla, key, len);
}


static int l2auth_parse_opt(
	struct link_util *link_util, int argc, char **argv, struct nlmsghdr *n)
{
	struct ilfa_l2auth_instructions instr = {};

	while (argc > 0) {
		if (matches(*argv, "key") == 0) {
			NEXT_ARG();
			addattr_key(argv, IFLA_L2AUTH_KEY, n);
			instr.set_key = 1;
		} else if (matches(*argv, "backup") == 0) {
			NEXT_ARG();
			addattr_key(argv, IFLA_L2AUTH_BACKUP_KEY, n);
			instr.set_backup_key = 1;
		} else if (matches(*argv, "del-backup") == 0) {
			instr.del_backup_key = 1;
		} else {
			invarg("unknown command", *argv);
		}
		NEXT_ARG_FWD();
	}
	addattr_l(n, L2AUTH_BUFLEN, IFLA_L2AUTH_INSTRUCTIONS, &instr,
	          sizeof(struct ilfa_l2auth_instructions));

	return 0;
}

static void l2auth_print_help(
	struct link_util *link_util, int argc, char **argv, FILE *file)
{
	if (argc > 0)
		fprintf(file, "Detailed help is not supported!\n\n");

	print_usage(file);
}


struct link_util l2auth_link_util = {
	.id = DRV_NAME,
	.maxattr = IFLA_L2AUTH_MAX,
	.parse_opt = l2auth_parse_opt,
	.print_help = l2auth_print_help,
};
