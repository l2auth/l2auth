local p_l2auth = Proto("l2auth", "Authenticated L2")

local f_real_ethertype = ProtoField.uint16("l2auth.real_ethertype", "Payload EtherType", base.HEX)
local f_hmac = ProtoField.bytes("l2auth.hmac", "HMAC")

p_l2auth.fields = { f_real_ethertype, f_hmac }

local distab = DissectorTable.get("ethertype")
local data_dis = Dissector.get("data")

-- l2auth header
local etype_len = 2
local l2a_hdr_len = etype_len

-- l2auth trailer
local l2a_hmac_len = 32
local l2a_tlr_len = l2a_hmac_len

function p_l2auth.dissector(buf, pkt, tree)
   -- Validate packet length
   local pkt_len = buf:len()
   if pkt_len < (l2a_hdr_len + l2a_tlr_len) then return end
   local payld_len = pkt_len - l2a_hdr_len - l2a_tlr_len

   -- Extract real ethertype
   local real_etype = buf(0, etype_len):uint()

   -- Construct tree
   local subtree = tree:add(p_l2auth, buf())
   subtree:add(f_real_ethertype, buf(0, etype_len))
   subtree:add(f_hmac, buf((l2a_hdr_len + payld_len), l2a_hmac_len))

   local payld_dis = distab:get_dissector(real_etype)

   if payld_dis ~= nil then
      -- Dissector was found, invoke subdissector with a new Tvb,
      -- created from the current buffer (skipping first two bytes).
      payld_dis:call(buf(2):tvb(), pkt, tree)
   else
      -- fallback dissector that just shows the raw data.
      data_dis:call(buf(2):tvb(), pkt, tree)
   end

   -- Set protocol name
   pkt.cols.protocol = string.format("%s [%s]",
                                     tostring(pkt.cols.protocol),  p_l2auth.name)

end

function p_l2auth.init()

   distab:add(tonumber("0x88e6"), p_l2auth)

end
